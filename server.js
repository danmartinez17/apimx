//Lista de dependencias
var bodyParser = require("body-parser"); //Parse incoming request bodies in a middleware before your handlers, available under the req.body property.
var path = require('path'); //Recursively require(...) files from a directory tree in Node.js
var requestjson = require("request-json"); //This lib aims to simplify Request usage for JSON only requests.

//Servidor express
var express = require('express'),
app = express(),
port = process.env.PORT || 3000;
app.listen(port);
console.log('todo list RESTful API server started on: ' + port);

//Lista de variables
var movimientosv2JSON = require('./movimientosv2.json');
var urlClientes = "https://api.mlab.com/api/1/databases/eballesterosf/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLab = requestjson.createClient(urlClientes);

//Confuguración de dependencias
app.use(bodyParser.json());

//Configuración de Headers
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

  //GET '/' method
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});
  //POST '/'metod
app.post('/', function (req, res) {
  res.send('Hemos recibido su petición post');
});
  //PUT '/' method
app.put('/', function (req, res) {
  res.send('Hemos recibido su petición put');
});
  //DELETE '/' method
app.delete('/', function (req, res) {
  res.send('Hemos recibido su petición delete cambiada');
});
app.get('/Clientes/:idcliente', function (req, res) {
  res.send('Aquí tiene al cliente número: ' + req.params.idcliente);
});
app.get('/v1/Movimientos', function (req, res) {
  res.sendfile('movimientosv1.json');
});
app.get('/v2/Movimientos', function (req, res) {
  res.json(movimientosv2JSON);
});
app.get('/v2/Movimientos/:id', function (req, res) {
  console.log(req.params.id);
  res.send(movimientosv2JSON[req.params.id]);
});
app.get('/v2/Movimientosquery', function (req, res) {
  console.log(req.query);
  res.send('Recibido: ' + req.query.nombre);
});
app.post('/v2/Movimientos', function (req, res) {
  var nuevo = req.body;
  nuevo.id = movimientosv2JSON.length + 1;
  movimientosv2JSON.push(nuevo);
  res.send('Movimiento dado de alta con id: ' + nuevo.id);
});

//Clientes, vienen de MLAB
app.get('/Clientes', function (req, res) {
  clienteMLab.get('', function (err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  });
});
app.post('/Clientes', function (req, res) {
  clienteMLab.post('', req.body, function (err, resM, body) {
    res.send(body);
  });
});