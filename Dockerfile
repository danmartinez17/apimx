# Imagen base node:<version>
FROM node:latest

#Directorio de la App en el contenedor
WORKDIR /app

#Copiado de archivos
ADD . /app

#Dependencias
RUN npm install

#Puerto expuesto
EXPOSE 3000

#Comando de ejecución de la aplicación
CMD [ "npm","start" ]